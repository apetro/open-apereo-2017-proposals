(Submitted).

Topic: Developing.

Title: MyUW for You: workshop edition

Audience level: 

+ Intermediate.

Audience type: 

+ Developer

Interest type:

+ Getting started
+ integration
+ Mobility
+ OpenSource
+ User Experience

Project type:

+ Incubator
+ uPortal

Preferred presentation method:

+ Pre-conference workshop half day


Presenters: 

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Timothy Vertein
University of Wisconsin-Madison
timothy.vertein@wisc.edu


Abstract:

MyUW is delivering a modern, responsive, designed campus portal user experience to UW-Madison and to other campuses in the University of Wisconsin system using uPortal and some free and open source add ons for uPortal enhancing its capabilities and implementing an alternative user interface layer.

This workshop is about demonstrating and getting hands-on with those add-ons, especially AngularJS-portal. AngularJS-portal implements

* the portal landing page, with dynamic widgets
* notifications and announcements
* a directory of applications
* search over that directory and beyond

Workshop participants will have the opportunity to work hands-on with these.

The workshop concludes with emphasizing the integration points between these add-ons and traditional uPortal (by exercising them), checking in on the Apereo incubation status of these add-ons, and teeing up next steps and conversations for seminar participants to consider implementing some or all of this goodness in their campus portals.
