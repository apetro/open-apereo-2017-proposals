(Submitted)

Topic: Developing.

Title: Whirlwind tour of MyUW

Audience level: All.

Audience type:

+ Administrator / Executive
+ Advocate
+ Developer
+ Newcomer
+ Technical / Functional manager

Interest type:

+ Accessibility
+ Awareness and advocacy
+ Getting started
+ Integration
+ Mobility
+ OpenSource
+ User Experience

Project type:

+ Incubator
+ uPortal

Preferred presentation format:

+ Lightning talk

Presenter: 

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Abstract:

Five minute lightning talk touring MyUW and emphasizing opportunities for your campus and portal to do likewise.

* Landing page
* Notifications
* Widgets
* Search
* App directory
* Application development framework
* Material design
* Call to action
