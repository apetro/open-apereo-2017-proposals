(Submitted).

Topic: Developing.

Title: BoF on Apereo security incident response

Audience level: 

+ Intermediate.

Audience type:

+ Developer
+ Technical / Functional Manager

Interest type:

+ OpenSource
+ Security

Project type:

+ uPortal
+ Other

Other types:

+ Cross-project





Types of audience/project/interest: 

+ Developer

Preferred presentation method: 

+ Birds of a Feather Discussion

Conveners: 

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Misagh Moayyed
mmoayyed@unicon.net
Identity and Access Management Consultant
Unicon, Inc.

Abstract:

Birds of Feather discussion of Apereo and Apereo project posture regarding security vulnerabilities and security incidents.

Practices:

+ Vulnerability reporting
+ Vulnerability handling
+ CVE-ID (Common Vulnerabilities and Exposures identifier) assignment and potential for Apereo to become a CVE Numbering Authority CNA).
+ Vulnerability disclosure
+ Practices for having fewer vulnerabilities

Perspectives:

+ What's working? 
+ What should be improved?
+ What are projects doing similarly that could be collaborated upon or combined?
+ Where are projects diverging in these practices and is that divergence valuable?
