(Submitted)

Topic: Developing.

Title: Universal Two-Factor authenticate all the things (universally?)

Audience level: Intermediate

Audience type:

+ Administrator / Executive
+ Advocate
+ Developer
+ Newcomer
+ Student
+ Technical / Functional manager

Interest type:

+ Awareness and advocacy
+ Getting started
+ Identity and Access Management
+ Integration
+ OpenSource
+ Security
+ TIER
+ User Experience

Project type:

+ CAS
+ Incubator
+ uPortal
+ Other : cross-project

Preferred presentation format:

+ Lightning talks

Presenter: 

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Abstract:

Five minute lightning talk examining the FIDO Universal Two-Factor (U2F) protocol, the state of its implementation in Apereo-related contexts, and the opportunity it presents for further implementation.

+ FIDO U2F what?
+ The algorithm in one minute.
+ Demo
+ Shibboleth support
+ CAS support
+ Towards risk-awareness in web application interactions
+ Call to action
