(Submitted)

Topic: Developing.

Title: MyUW and You: progress in AngularJS-portal and how you can too

Audience level: Intermediate.

Audience type:

+ Developer
+ Technical / Functional manager

Interest type:

+ Accessibility
+ Getting Started
+ Integration
+ Mobility
+ OpenSource
+ User Experience

Project type:

+ Incubator
+ uPortal

Preferred presentation method:

+ Presentation

Presenters: 

Jim Helwig
University of Wisconsin-Madison
jim.helwig@wisc.edu

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Timothy Vertein
University of Wisconsin-Madison
timothy.vertein@wisc.edu

Abstract:

MyUW is delivering a modern, responsive, designed campus portal user experience. This presentation is about what Madison is doing and how you can too.

+ Summary of recent progress in `AngularJS-portal` and the other supported free and open source add-on software modules.
+ What's working for us in MyUW, and what we're working on.
+ Roadmap
+ How you can adopt parts or all of this goodness today.
