This repository exists to publicly, transparently share and collaborate on presentation proposals for Open Apereo 2017.

# Submitted:

## Pre-conference workshop

+ MyUW for You: workshop edition

## Birds of a feather

+ BoF on Apereo security incident response

## Presentations

+ MyUW and You: progress in AngularJS-portal and how you can too
+ Two hard problems, in vignettes.

## Lighting talk

+ On Common Vulnerabilities and Exposures Identifiers (CVE IDs)
+ Static site generators for the win
+ Universal Two-Factor authenticate all the things (universally?)
+ Whirlwind tour of MyUW
