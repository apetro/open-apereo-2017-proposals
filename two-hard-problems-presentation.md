(Submitted.)

Topic: Developing.

Abstract Title: Two hard problems, in vignettes

Level of audience: Intermediate.

Keywords: portal, uPortal, AngularJS, framework.

Audience type:

+ Developer

Interest type: 

+ Accessibility
+ Other

Project type:

+ uPortal

Presentation Needs:

+ Computer audio

Preferred presentation method: 

+ Presentation

Presenters: 

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Abstract:

There are two hard problems in programming: caching, naming things, and off by one errors.

This presentation will use that joke as a structure for sharing interesting anecdotes from professional programming in higher education, starting from humor and transitioning to critical consideration of hard problems and what to do about them.
