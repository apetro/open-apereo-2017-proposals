(Submitted).

Topic: Developing.

Title: Static site generators for the win

Audience level: Beginner

Audience type:

+ Administrator / Executive
+ Advocate
+ Developer
+ Newcomer
+ Student
+ Technical / Functional manager

Interest type:

+ Awareness and advocacy
+ Documentation
+ Getting started
+ OpenSource
+ Security

Project type:

+ Incubator
+ uPortal
+ Other : cross-project

Preferred presentation format:

+ Lightning talks

Presenter: 

Andrew Petro
andrew.petro@wisc.edu
Software Developer, Division of Information Technology
UW-Madison 

Abstract:

Five minute lightning talk examining static site generators, especially in the context of Apereo projects.

+ What's a static site generator?
+ Examples
+ Why is this important: Scalability and performance?
+ Why is this important: Cost?
+ Why is this important: Security?
+ Why is this important: Git[Hub|Lab] syntactic sugar?
+ Why is this important: Openness.
+ Call to action
